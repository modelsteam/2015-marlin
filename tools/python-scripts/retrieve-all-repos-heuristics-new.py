import subprocess
import os
import csv
import urllib2
import re
import MySQLdb


db = MySQLdb.connect(host="localhost", user="", passwd="",db="marlin_icmse")
cursor = db.cursor()
a=re.compile("([a-z0-9]){40}")

cfgfile="Configuration.h"
cfgfile1="Configuration_adv.h"
marlinfolder="ErikZalm-Marlin"

result=[['repo name,branch-name,configuration,features,features-propagated,bug-fixes,bug-fixes-propagated,sync-with-upstream,nr_of_diff_authors']]

feature=['add','support','feature','new', 'added','implemented']
bug=['corrected','fix','bugfix', 'bug','fixed','replace']

pull_requests=False
nr_of_diff_authors = 0


def getcommits(branch, repositoryfolder, repo_creation_date):
	return subprocess.Popen(['git','log','--since="'+repo_creation_date+'"','--pretty=oneline'], stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")

def getownercommits(branch, repositoryfolder, repo_creation_date, author):
	command = 'git log --since='+repo_creation_date+' --pretty=oneline --author="'+author+'"'
	return subprocess.Popen(command,universal_newlines=True, shell=True,stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")


def modifiedfiles(commit,repositoryfolder):
	cmd=subprocess.Popen(['git','diff-tree', '--no-commit-id', '--name-only', '-r', commit],stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")
	return cmd
	
def getauthors(repositoryfolder):
	git_authors_cmd = 'git log --all --format="%aN<!--!>%cE"'
	return subprocess.Popen(git_authors_cmd,stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")

def setauthor(repo_owner,owner_name, owner_email):
	repo_owner = repo_owner.strip('"')
	owner_name = owner_name.strip('"')
	author= "\("+repo_owner+"\)"
	if owner_name and not owner_name.isspace():
		author= "\("+repo_owner+"\)\|\("+owner_name+"\)"
	if owner_email and not owner_email.isspace():
		author= "\("+repo_owner+"\)\|\("+owner_name+"\)\|\("+owner_email+"\)"
	elif owner_email and not owner_email.isspace():
		author= "\("+repo_owner+"\)\|\("+owner_email+"\)"
					
	return author
	
marlin_commits = getcommits("Marlin_v1",marlinfolder,"2011-08-13T08:07:20Z")	
marlin_authors = set(getauthors(marlinfolder))

resultfile = open('results.csv', 'w')
writer = csv.writer(resultfile, dialect='excel')

	
with open('list_of_forks_and_dates.csv', 'rb') as csvfile:
    repositories = csv.reader(csvfile, delimiter=' ')
    for repo in repositories:
		comma = repo[0].count(',')
		text = ""
		if(comma == 5):
			matches=re.findall(r'\"(.+?)\"',repo[0])
			text = ",".join(matches)
			if(',' in text):
				newtext = text.replace(',',' ')
				repo[0] = repo[0].replace(str(text), str(newtext))

		repo_split = repo[0].split(",", 5)
		repo_split += [None] * (5 - len(repo_split))
		repository, repo_creation_date,repo_owner,owner_name,owner_email = repo_split
	
		print repository
		print repo_creation_date
		#clone the repository
		repo_link = "https://github.com/"+repository+".git"
		repo_folder = repository.replace("/","-")
		
		# authors of ErikZalm
		# get authors of this branch and intersect with authors of ErikZalm/Marlin. There may be users that do not match their github account
		# e.g 33d - Damien is the committer
		
		# repository pull-requests
		cursor.execute('SELECT pullrequest_number,state,merged,nr_of_commits FROM marlin_icsme.pull_requests WHERE repo_name = "ErikZalm/Marlin" AND from_repo_name="'+repository+'"')
		rows = cursor.fetchall()
		for row in rows:
			print row
		if(len(rows) != 0):
			pull_requests = True
		#Check if repository exists
		repo_code = 200
		try:
			repo_code = urllib2.urlopen(repo_link).getcode()
		except urllib2.HTTPError as httpError:
			repo_code = httpError.code
		if(repo_code != 404):
			try:
				subprocess.check_call(['git','clone',repo_link,repo_folder])
			except subprocess.CalledProcessError as error:
				errorcode = error.returncode
				if(errorcode == 128):
					print("Repository already exists")
			
			#authors of ErikZalm/Marlin and current repository
			repo_authors = set(getauthors(repo_folder))
			different_authors = repo_authors.difference(marlin_authors)
		
			#if this is the first time for retrieval, we need to do a fetch before checking out all branches
			git_fetch = subprocess.Popen(['git','fetch'], cwd=os.getcwd() + "/" + repo_folder)
			git_fetch.wait()
		
			#Get branches
			remote_branches = subprocess.Popen(["git","branch", "-r"], stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repo_folder).communicate()[0].split("\n")
			for branch in filter(None,remote_branches):
				branch_name = branch.split("/",1)[-1]
				if "HEAD ->" not in branch_name:
					print(branch_name)
					b = subprocess.Popen(['git','checkout',branch_name], cwd=os.getcwd() + "/" + repo_folder,stderr=subprocess.STDOUT, stdout=subprocess.PIPE).communicate()[0]
					#b.wait()
					#get all commits after repository creation date
					
					#construct the regex for commit author - owner of repository, name, e-mail
					author= setauthor(repo_owner,owner_name,owner_email)
					
					# Features
					features=False
					propagatedfeatures=False
					bugs=False
					propagatedbugs=False
					sync=False
					# Commits
					commits = getownercommits(branch_name,repo_folder,repo_creation_date,author)
					if(len(commits) == 1):
						
						#if there are more than one different authors, keep track of that and output to final document. This is for finer granularity (there
						#may be repositories which are not forked from ErikZalm/Marlin and they have other commits than EzMarlin, which are from their parents)
						if(len(different_authors) !=0):
							owner_name,owner_email = different_authors.pop().split("<!--!>")
							author = setauthor(repo_owner,owner_name,owner_email)
							commits = getownercommits(branch_name,repo_folder,repo_creation_date,author)
						nr_of_diff_authors = len(different_authors)
						#print(different_authors)
					#print author
					config = False
					if(len(commits)>1):
						for commit in commits: 
							if(a.match(commit)):
								sha, message = commit.split(" ",1)
								message = message.lower()
								#print sha + "," + message
								# Configuration files
								filesmodified += filter(None,modifiedfiles(sha,repo_folder))
								#print(filesmodified)
								#if any(cfgfile in string for string in filesmodified or cfgfile1 in string for string in filesmodified):
								
								# Features
								if any ( f for f in feature if f in message and "enabled" not in message):
									#if there have been modified other files than configuration files, then it may be a feature
									features = True
									#Check if the feature was propagated
									if (pull_requests and commit in marlin_commits):
										propagatedfeatures = True
										
								#Bugs
								if any (b for b in bug if b in message):
									#print("Bug fixes on this branch")
									bugs = True
									#Check if the bugs were propagated
									if pull_requests and commit in marlin_commits:
										propagatedbugs = True
								
								#Sync with upstream
								repo_commits = set(getcommits(branch,repo_folder,repo_creation_date))
								if (len(repo_commits.intersection(set(marlin_commits))) != 1):
									sync = True
								else:
									sync = False
						#end of commits loop	
						if any (cfgfile in string for string in filesmodified):
							config = True
						if any (cfgfile1 in string for string in filesmodified):
							config = True
					#result	
					result.append([repo_folder +","+branch_name+","+str(config)+","+str(features) +"," +str(propagatedfeatures) + "," + str(bugs) +","+ str(propagatedbugs) + "," + str(sync) + "," + str(nr_of_diff_authors)])
					
writer.writerows(result)					
resultfile.close()			