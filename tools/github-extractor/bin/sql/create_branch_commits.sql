create table if not exists branches_commits(
	branch_commit_id INT PRIMARY KEY auto_increment,
	commit_id INT,
	branch_id INT,
	CONSTRAINT fk_commit_id_constraint
	FOREIGN KEY fk_commit_id (commit_id)
	REFERENCES commits(id) 
	ON UPDATE CASCADE
	ON DELETE RESTRICT
	
);
