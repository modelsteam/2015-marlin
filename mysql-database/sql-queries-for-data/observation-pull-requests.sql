SELECT * FROM marlin_icsme.pull_requests WHERE repo_id = 1;

#Nr of pull-requests accepted
SELECT COUNT(*) FROM marlin_icsme.pull_requests where repo_id = 1 AND merged = 1;

# Average days for closing a pull request, that is merged
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1  AND  merged = TRUE;


#All preqs
SELECT id, COUNT(*) AS Total, COUNT(*) / (SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1) * 100 AS 'Percentage to all merged' FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1; 
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1;

#Preqs with fewer commits have a higher acceptance rate. 

# >3 commits
SELECT id, COUNT(*) AS Total, COUNT(*) / (SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND nr_of_commits > 3) * 100 AS 'Percentage to all merged' FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits > 3; 
SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND nr_of_commits > 3; #total
SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits > 3; #merged
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits > 3;
SELECT AVG (additions) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits > 3;
SELECT AVG (deletions) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits > 3;

# <=3 commits
SELECT id, COUNT(*) AS Total, COUNT(*) / (SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND nr_of_commits <= 3) * 100 AS 'Percentage to all merged' FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3; 
SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND nr_of_commits <= 3; #total
SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3; #merged
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3;
SELECT AVG (additions) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3;
SELECT AVG (deletions) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3;

# =1 commits
SELECT id, COUNT(*) AS Total, COUNT(*) / (SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND nr_of_commits =3 ) * 100 AS 'Percentage to all merged' FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits =3; 
SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND nr_of_commits <= 3; #total
SELECT COUNT(*) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3; #merged
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3;

SELECT AVG (additions) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged = 1 AND nr_of_commits <= 3;

#All the active forks which have created a preq
SELECT * FROM marlin_icsme.repository WHERE repo_name IN (SELECT from_repo_name FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND from_repo_name != 'unknown') AND active_fork = 1;
