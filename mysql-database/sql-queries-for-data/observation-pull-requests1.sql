SELECT * FROM marlin_icsme.pull_requests WHERE repo_id = 1  AND state='open';
#Level 1 forks
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 1) AND repo_id = 1 AND state = 'closed' AND merged = 0;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 1) AND repo_id = 1  AND merged=1;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 1) AND repo_id = 1  AND state='open';
#NR of level 1 forks
SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 1;
SELECT DISTINCT(from_repo_name) FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 1) AND repo_id = 1;

#Level 2 forks
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 2) AND repo_id = 1 AND state = 'closed' AND merged = 0;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 2) AND repo_id = 1  AND merged=1;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 2) AND repo_id = 1  AND state='open';
SELECT DISTINCT(from_repo_name) FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 2) AND repo_id = 1;
#Level 3 forks
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 3) AND repo_id = 1 AND state = 'closed' AND merged = 0;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 3) AND repo_id = 1  AND merged=1;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 3) AND repo_id = 1  AND state='open';


#Level 4 forks
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 4) AND repo_id = 1 AND state = 'closed' AND merged = 0;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 4) AND repo_id = 1  AND merged=1;
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork=1 AND fork_level = 4) AND repo_id = 1  AND state='open';
#to other marlin_icsme.repository
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT repo_name FROM marlin_icsme.repository WHERE active_fork = 1 AND repo_id !=1 ) AND repo_name != from_repo_name;

# unknown forks
SELECT * FROM marlin_icsme.pull_requests WHERE from_repo_name = 'unknown' AND repo_id = 1 AND state = 'closed' AND merged = 0;
SELECT * FROM marlin_icsme.pull_requests WHERE from_repo_name = 'unknown'AND repo_id = 1  AND merged=1;
SELECT * FROM marlin_icsme.pull_requests WHERE from_repo_name = 'unknown' AND repo_id = 1  AND state='open';
SELECT DISTINCT(user_login) FROM marlin_icsme.pull_requests WHERE from_repo_name = 'unknown' AND repo_id = 1;

#All the active forks which have created a preq
SELECT DISTINCT(repo_name) FROM marlin_icsme.repository WHERE repo_name IN (SELECT from_repo_name FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND from_repo_name != 'unknown') AND active_fork = 1;
