# Pull requests to main Marlin from forks that created commits with #ifdefs annotations
SELECT * FROM pull_requests WHERE from_repo_name IN (SELECT DISTINCT(repository) FROM repository_forks_erikzalm_marlin.analysis_ifdef) AND repo_id = 1;

# Forks that created commits with #ifdefs annotation
SELECT DISTINCT(repository) FROM repository_forks_erikzalm_marlin.analysis_ifdef;

# Forks that created commits with #ifdefs annotations and created pull requests to Marlin
SELECT DISTINCT(from_repo_name) FROM pull_requests WHERE from_repo_name IN (SELECT DISTINCT(repository) FROM repository_forks_erikzalm_marlin.analysis_ifdef) AND repo_id = 1;