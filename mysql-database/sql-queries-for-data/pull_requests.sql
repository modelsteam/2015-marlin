# Marlin marlin_icsme.pull_requests
SELECT * FROM marlin_icsme.pull_requests where repo_id = 1;

# Marlin pull requests this year
SELECT * FROM marlin_icsme.pull_requests where repo_id = 1 AND created_at > TIMESTAMP('2014-01-01T00:00:00Z');
SELECT * FROM marlin_icsme.pull_requests where repo_id = 1 AND created_at > TIMESTAMP('2014-01-01T00:00:00Z');

#How many repositories were created this year?
SELECT * FROM marlin_icsme.repository WHERE created_at > TIMESTAMP('2014-01-01T00:00:00Z');

# CLOSED PULL REQUESTS
SELECT * FROM marlin_icsme.pull_requests where state = "closed" and repo_id = 1;

# CLOSED PULL REQUESTS BUT NOT MERGED
SELECT * FROM marlin_icsme.pull_requests where state = "closed" and repo_id = 1 AND merged=0; 

# MERGED PULL REQUESTS
SELECT * FROM marlin_icsme.pull_requests where repo_id = 1 AND merged = 1;

# NON MERGED PULL REQUESTS
SELECT * FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND merged=0;

# OPEN PULL REQUESTS
SELECT * FROM marlin_icsme.pull_requests WHERE repo_id=1 AND state='open';

# All pull requests that were closed after half of year
SELECT * FROM marlin_icsme.pull_requests WHERE repo_id=1 AND state='closed' AND merged = 0 AND DATEDIFF(closed_at,created_at) > 180;

# Average days for closing a pull request
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1 AND closed_at != 'Null';

# Average days for closing a pull request, that is merged
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1  AND  merged = TRUE;

SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1 AND closed_at != 'Null';

# Average days for closing a pull request, that is not merged
SELECT AVG (DATEDIFF (closed_at, created_at)) from marlin_icsme.pull_requests WHERE repo_id = 1 AND closed_at != 'Null' AND merged = TRUE;

# All the repositories that have created pull-requests for Marlin
SELECT * FROM marlin_icsme.repository WHERE(repo_name) IN (SELECT (from_repo_name) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND marlin_icsme.repository.repo_name = from_repo_name) AND marlin_icsme.repository.id != 1 LIMIT 10000;

# All that did not create a pull request
SELECT * FROM marlin_icsme.repository WHERE(repo_name) NOT IN (SELECT (from_repo_name) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND marlin_icsme.repository.repo_name = from_repo_name) AND marlin_icsme.repository.id != 1 LIMIT 10000;

# All alive repositories that have created marlin_icsme.pull_requests for Marlin
SELECT * FROM marlin_icsme.repository WHERE(repo_name) IN (SELECT (from_repo_name) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND marlin_icsme.repository.repo_name = from_repo_name AND marlin_icsme.repository.active_fork = TRUE) AND marlin_icsme.repository.id != 1 AND marlin_icsme.repository.fork_level > 1 LIMIT 10000;

# All alive repositories that have not created a pull request for Marlin
SELECT * FROM marlin_icsme.repository WHERE(repo_name) NOT IN (SELECT (from_repo_name) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND marlin_icsme.repository.repo_name = from_repo_name AND marlin_icsme.repository.active_fork = TRUE) AND marlin_icsme.repository.id != 1 LIMIT 10000;

# All alive repositories that have created a pull request and their are not direct forks of Marlin
SELECT * FROM marlin_icsme.repository WHERE(repo_name) IN (SELECT (from_repo_name) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND marlin_icsme.repository.repo_name = from_repo_name AND marlin_icsme.repository.active_fork = TRUE) AND marlin_icsme.repository.id != 1 AND marlin_icsme.repository.fork_level = 1 LIMIT 10000;

SELECT * FROM marlin_icsme.repository WHERE(repo_name) IN (SELECT (from_repo_name) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND marlin_icsme.repository.repo_name = from_repo_name AND marlin_icsme.repository.active_fork = TRUE) AND marlin_icsme.repository.id != 1 AND marlin_icsme.repository.fork_level = 1 LIMIT 10000;

# All repositories that have created a pull_request
#SELECT * FROM marlin_icsme.repository WHERE(repo_name) IN (SELECT (repo_name) FROM marlin_icsme.pull_requests WHERE repo_id = 1 AND marlin_icsme.repository.repo_name = marlin_icsme.pull_requests.from_repo_name)

# All repositories that have created a pull_request
#SELECT marlin_icsme.repository.repo_name FROM marlin_icsme.repository i JOIN (SELECT * FROM marlin_icsme.pull_requests WHERE repo_id = 1 ) AS T1 ON i.repo_name = T1.repo_name;
