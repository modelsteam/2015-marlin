#Comparing heuristics with manual analysis for configuration, features and bug-fixes

#Configuration
SELECT * FROM marlin_icsme.forks_analysis_manually AS f1 INNER JOIN marlin_icsme.forks_analysis_heuristics  AS f2 ON f1.repo_name = f2.repo_name AND f1.branch_name = f2.branch_name AND f1.configuration = f2.configuration ;
#Features
SELECT * FROM marlin_icsme.forks_analysis_manually AS f1 INNER JOIN marlin_icsme.forks_analysis_heuristics  AS f2 ON f1.repo_name = f2.repo_name AND f1.branch_name = f2.branch_name AND f1.features = f2.features;
#Bug-Fixes
SELECT * FROM marlin_icsme.forks_analysis_manually AS f1 INNER JOIN marlin_icsme.forks_analysis_heuristics  AS f2 ON f1.repo_name = f2.repo_name AND f1.branch_name = f2.branch_name AND f1.bug_fixes = f2.bug_fixes;