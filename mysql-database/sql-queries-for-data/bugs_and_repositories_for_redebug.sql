SELECT * FROM marlin_icsme.commits WHERE repo_id = 1 AND message REGEXP "break" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');
SELECT * FROM marlin_icsme.commits WHERE repo_id = 1 AND message REGEXP "bug" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');
SELECT * FROM marlin_icsme.commits WHERE repo_id = 1 AND message REGEXP "crash" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');
SELECT * FROM marlin_icsme.commits WHERE repo_id = 1 AND message REGEXP "fix" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');

SELECT * FROM marlin_icsme.commits WHERE repo_id != 1 AND message REGEXP "bug" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');
SELECT * FROM marlin_icsme.commits WHERE repo_id != 1 AND message REGEXP "crash" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');
SELECT * FROM marlin_icsmecommits WHERE repo_id != 1 AND message REGEXP "fix" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');
SELECT * FROM marlin_icsme.commits WHERE repo_id != 1 AND message REGEXP "break" AND DATE(committer_date) > DATE('2014-01-01T00:00:00Z');
